This is a simple web application that is made to check if Gitlab Pages is OK
with HTTP Range requests.

That's it. Available online [here](https://pmdias.gitlab.io/does_gitlab_ranges/).

## Explanation

This small project add the sole purpose to test that it is possible to deploy
content on Gitlab Pages and retrieve that content making use of
[HTTP Range requests](https://developer.mozilla.org/en-US/docs/Web/HTTP/Headers/Range).
This is an exploratory personal project that will feed some other personal projects
that will make use of HTTP Range requests.


## Notes

In order to enable HTTP Range requests on Gitlab Pages, it seems that we need to add the
following variables to the Gitlab CI job that deploys the pages:

```yaml
variables:
  FF_USE_FASTZIP: "true"
  ARTIFACT_COMPRESSION_LEVEL: "fastest"
```

This information comes from [this](https://docs.gitlab.com/ee/user/project/pages/introduction.html#cannot-play-media-content-on-safari)
page of the official Gitlab Pages documentation.
