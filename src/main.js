const firstLineBtn = document.getElementById('first-line-load-btn');
const firstLineOutput = document.getElementById('first-line-output');

const fetchWithRange = async (range_start, range_end) => {
  const response = await fetch('https://pmdias.gitlab.io/does_gitlab_ranges/content.txt', {
    headers: { Range: `bytes=${range_start}-${range_end}`}
  });

  const text = await response.text();

  firstLineOutput.innerText = response.status >= 200 && response.status < 400
    ? text
    : 'There was an error :(';
}

firstLineBtn.addEventListener('click', () => fetchWithRange(0, 23));
